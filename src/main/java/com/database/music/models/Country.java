package com.database.music.models;

/* Self explaining class with the data needed from Country
 */
public class Country {
    private String name;
    private int count;

    public Country(String name, int count) {
        this.name = name;
        this.count = count;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
}
