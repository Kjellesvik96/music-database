package com.database.music.models;

/* Made this additional class to retrieve all the info
 * to display on the search page
 */
public class SearchedTrack {
    private int trackId;
    private String trackTitle;
    private int albumId;
    private String albumTitle;
    private int genreId;
    private String genreTitle;
    private String artistTitle;

    public SearchedTrack(String trackTitle, String albumTitle, String genreTitle, String artistTitle) {
        this.trackTitle = trackTitle;
        this.albumTitle = albumTitle;
        this.genreTitle = genreTitle;
        this.artistTitle = artistTitle;
    }
    public int getTrackId() {
        return trackId;
    }
    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }
    public String getTrackTitle() {
        return trackTitle;
    }
    public void setTrackTitle(String trackTitle) {
        this.trackTitle = trackTitle;
    }
    public int getAlbumId() {
        return albumId;
    }
    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }
    public String getAlbumTitle() {
        return albumTitle;
    }
    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }
    public int getGenreId() {
        return genreId;
    }
    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }
    public String getGenreTitle() {
        return genreTitle;
    }
    public void setGenreTitle(String genreTitle) {
        this.genreTitle = genreTitle;
    }
}
