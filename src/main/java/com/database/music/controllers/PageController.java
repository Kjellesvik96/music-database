package com.database.music.controllers;

import com.database.music.data_access.CustomerRepository;
import com.database.music.models.Customer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/* A Controller class for all the different page queries.
 * There is still a bug in the add-customer query..
 */
@Controller
public class PageController {

    CustomerRepository customerRepository = new CustomerRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("tracks", customerRepository.getTopFiveTracks());
        model.addAttribute("artists", customerRepository.getTopFiveArtists());
        model.addAttribute("genres", customerRepository.getTopFiveGenres());
        return "index";
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String getAllCustomers(Model model) {
        model.addAttribute("customers", customerRepository.getAllCustomers());
        return "view-customers";
    }

    @RequestMapping(value = "/customers/{customerId}")
    public String getSpecificCustomer(@PathVariable int customerId, Model model) {
        model.addAttribute("customer", customerRepository.getSpecificCustomer(customerId));
        return "view-customer";
    }

    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public String getCountries(Model model) {
        model.addAttribute("countries", customerRepository.getCustomersOrderedByCountry());
        return "view-countries";
    }

    @RequestMapping(value = "/add-customer", method = RequestMethod.GET)
    public String addCustomer(Model model) {
        model.addAttribute("customer", new Customer());
        return "add-customer";
    }

    @RequestMapping(value = "/add-customer", method = RequestMethod.POST)
    public String addCustomer(@ModelAttribute Customer customer, BindingResult error, Model model){
        try {
            Boolean success = customerRepository.addCustomer(customer);
            model.addAttribute("success", success);
            if (success) {
                model.addAttribute("customer", new Customer());
            }
        } catch (Exception e){
            System.out.println(e.toString());
        }
        return "add-customer";
    }

}
