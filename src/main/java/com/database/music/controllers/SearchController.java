package com.database.music.controllers;

import com.database.music.data_access.CustomerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


/* Had some issues trying to put this in the PageController page
 * Found it easier to make it a separate Class
 */
@Controller
public class SearchController {
    CustomerRepository customerRepository = new CustomerRepository();
    @RequestMapping("/search")
    public String index(@RequestParam(value = "searchQuery") String search,
                        Model model) {
        model.addAttribute("searchResults", customerRepository.getSongBySearch(search));
        return "search";
    }
}
