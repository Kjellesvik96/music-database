package com.database.music.controllers;

import com.database.music.data_access.CustomerRepository;
import com.database.music.models.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/* A Controller class for all the data base queries.
 */
@RestController
public class CustomerController {
    CustomerRepository customerRepository = new CustomerRepository();

    @RequestMapping(value= "/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value= "/api/customers/{customerId}", method = RequestMethod.GET)
    public Customer getSpecificCustomer(@PathVariable int customerId) {
        return customerRepository.getSpecificCustomer(customerId);
    }

    @RequestMapping(value= "/api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @RequestMapping(value= "/api/customers", method = RequestMethod.PUT)
    public Boolean updateCustomer(@RequestBody Customer customer) {
        return  customerRepository.updateCustomer(customer);
    }

    @RequestMapping(value= "/api/customers/countries", method = RequestMethod.GET)
    public ArrayList<Country> getCustomersOrderedByCountry() {
        return customerRepository.getCustomersOrderedByCountry();
    }

    @RequestMapping(value= "/api/customers/spenders", method = RequestMethod.GET)
    public ArrayList<Customer> getBigSpenders() {
        return customerRepository.getBigSpenders();
    }

    @RequestMapping(value= "/api/customers/genre", method = RequestMethod.GET)
    public String getTopGenre(String customerID) {
        return customerRepository.getTopGenre(customerID);
    }

    @RequestMapping(value= "/api/track", method = RequestMethod.GET)
    public ArrayList<Track> getTopFiveTracks() {
        return customerRepository.getTopFiveTracks();
    }

    @RequestMapping(value= "/api/artist", method = RequestMethod.GET)
    public ArrayList<Artist> getTopFiveArtists() {
        return customerRepository.getTopFiveArtists();
    }

    @RequestMapping(value= "/api/genre", method = RequestMethod.GET)
    public ArrayList<Genre> getTopFiveGenres() {
        return customerRepository.getTopFiveGenres();
    }
}
