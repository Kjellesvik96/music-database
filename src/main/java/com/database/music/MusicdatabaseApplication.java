package com.database.music;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicdatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(MusicdatabaseApplication.class, args);
    }

}
