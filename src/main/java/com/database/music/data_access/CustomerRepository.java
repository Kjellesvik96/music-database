package com.database.music.data_access;

import com.database.music.models.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CustomerRepository {

    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    //CRUD
    /* Below are all the different queries made to the database.
     * The first 5 queries are "Customer Queries"
     * The last 4 queries are "Track Queries"
     * The bodies to all the queries are pretty similar.
     */
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, " +
                                                                "PostalCode, Phone, Email FROM Customer");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                customers.add( new Customer(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone"),
                        set.getString("Email")
                ));
            }
            System.out.println("Get All went well");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return customers;
    }

    public Customer getSpecificCustomer(int customerId) {
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, " +
                                                                "PostalCode, Phone, Email FROM Customer WHERE CustomerId=?");
            prep.setInt(1, customerId);
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                customer = new Customer(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone"),
                        set.getString("Email")
                );
            }
            System.out.println("Get Specific went well");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }

        return customer;
    }

    public Boolean addCustomer(Customer customer) {
        Boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("INSERT INTO Customer(CustomerId, FirstName, LastName, " +
                                                                "Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?,?)");
            prep.setInt(1, customer.getCustomerID());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCountry());
            prep.setString(5, customer.getPostalCode());
            prep.setString(6, customer.getPhone());
            prep.setString(7, customer.getEmail());

            int result = prep.executeUpdate();
            success = (result != 0); //if res = 1; true

            System.out.println("ADD went well");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    public Boolean updateCustomer(Customer customer) {
        Boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("UPDATE Customer SET CustomerId=?, FirstName=?, " +
                                                "LastName=?, Country=?, PostalCode=?, Phone=?, Email=? WHERE CustomerId=?");
            prep.setInt(1, customer.getCustomerID());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCountry());
            prep.setString(5, customer.getPostalCode());
            prep.setString(6, customer.getPhone());
            prep.setString(7, customer.getEmail());
            prep.setInt(8, customer.getCustomerID());

            int result = prep.executeUpdate();
            success = (result != 0); //if res = 1; true

            System.out.println("Update went well");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    public ArrayList<Country> getCustomersOrderedByCountry() {
        ArrayList<Country> countries = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT COUNT(*) AS count, country " +
                            "FROM customer " +
                            "GROUP BY country " +
                            "ORDER BY count DESC");
            ResultSet resultSet = prep.executeQuery();
            while(resultSet.next()) {
                countries.add(new Country(
                        resultSet.getString("country"),
                        resultSet.getInt("count")
                ));
            }
        } catch(Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return countries;
    }

    public ArrayList<Customer> getBigSpenders() {
        ArrayList<Customer> customers = new ArrayList<>();
        // Trenger litt hjelp her
        return customers;
    }

    public String getTopGenre(String customerID) {
        String genre = null;
        // Ser veldig vanskelig ut
        return genre;
    }

    public ArrayList<Track> getTopFiveTracks() {
        ArrayList<Track> topFiveTracks = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT TrackId, Name, Composer FROM Track ORDER BY random() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                topFiveTracks.add( new Track(
                        set.getInt("TrackId"),
                        set.getString("Name"),
                        set.getString("Composer")
                ));
            }
            System.out.println("Get 5 tracks went well");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return topFiveTracks;
    }

    public ArrayList<Artist> getTopFiveArtists() {
        ArrayList<Artist> topFiveArtists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT ArtistId, Name FROM Artist ORDER BY random() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                topFiveArtists.add( new Artist(
                        set.getInt("ArtistId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get 5 artists went well");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return topFiveArtists;
    }

    public ArrayList<Genre> getTopFiveGenres() {
        ArrayList<Genre> topFiveGenres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT GenreId, Name FROM Genre ORDER BY random() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                topFiveGenres.add( new Genre(
                        set.getInt("GenreId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get 5 genres went well");

        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return topFiveGenres;
    }

    public ArrayList<SearchedTrack> getSongBySearch(String searchQuery) {
        ArrayList<SearchedTrack> songs = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT Track.Name as Track, Album.Title as Album, Genre.Name as Genre, Artist.Name as Artist " +
                            "FROM Track " +
                            "INNER JOIN Album " +
                            "ON Track.AlbumId = Album.AlbumId " +
                            "INNER JOIN Genre " +
                            "ON Track.GenreId = Genre.GenreId " +
                            "INNER JOIN Artist " +
                            "ON Album.ArtistId = Artist.ArtistId " +
                            "WHERE Track LIKE ?");
            prep.setString(1, searchQuery + "%");
            ResultSet resultSet = prep.executeQuery();
            while(resultSet.next()) {
                songs.add(new SearchedTrack(
                        resultSet.getString("Track"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre"),
                        resultSet.getString("Artist")
                ));
            }
        } catch(Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch(Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return songs;
    }
}
